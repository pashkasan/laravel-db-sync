<?php

namespace MWI\LaravelDBSync;

use Illuminate\Support\ServiceProvider;

class DBSyncServiceProvider extends ServiceProvider
{

    public function boot() {
        parent::boot();
    }


    protected $commands = [
        'MWI\LaravelDBSync\Commands\DBSync',
    ];

    /*        public function boot(\Illuminate\Routing\Router $router) {
            $this->commands([
                'MWI\LaravelDBSync\Commands\DBSync'
            ]);*/

    public function register()
    {
        $this->commands($this->commands);
    }

    /*        if ($this->app->runningInConsole()) {
                $this->commands([
                   Commands\DBSync::class
                ]);
            }*/
}