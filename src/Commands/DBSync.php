<?php

namespace MWI\LaravelDBSync\Commands;

use Illuminate\Console\Command;
use DB;
use App;
use File;
use phpseclib\Net\SSH2;
use phpseclib\Net\SFTP;

class DBSync extends Command
{
    /**
     * The name and signature of the console command.x
     *
     * @var string
     */
    protected $signature = 'db:sync';

    /**
     * The console command description.
     *s
     * @var string
     */
    protected $description = 'Allows you to sync your local database to your remote.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public static function format($string, $replaceArray)
    {

        $replace = [];
        foreach ($replaceArray as $idx => $item) {
            $replace[sprintf('{%s}', $idx)] = $item;
        }

        return strtr($string, $replace);

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $remote_url = env('REMOTE_SYNC_URL');

        $remote_db = env('REMOTE_SYNC_DB_NAME');

        $remote_db_host = env('REMOTE_SYNC_DB_HOST');
        $remote_db_port = env('REMOTE_SYNC_DB_PORT');

        $remote_db_user = env('REMOTE_SYNC_DB_USER');
        $remote_db_pass = env('REMOTE_SYNC_DB_PASS');

        $remote_db_tables = env('REMOTE_SYNC_DB_TABLES');

        $ssh_user = env('REMOTE_SYNC_SSH_USERNAME');
        $ssh_pass = env('REMOTE_SYNC_SSH_PASSWORD');

        // Checking to make sure this isn't production.
        if (App::environment('production')) {
            $this->error("Please don't try and run this in production... will not end well.");
            return;
        }

        if (!$remote_db || !$remote_db || !$ssh_user || !$ssh_pass) {
            $this->error('Add your environment variables!');
            return;
        }

        // Connect via ssh to dump the db on the remote server.
        $ssh = new SSH2($remote_url);
        if (!$ssh->login($ssh_user, $ssh_pass)) {
            $this->error('Login failed make sure your ssh username and password is set in your env file.');
            return;
        }

        if ($remote_db_host) {

            $ssh->exec(self::format(
                'mysqldump -h {remote_db_host} -P {remote_db_port} -u {remote_db_user} -p{remote_db_pass} {remote_db} {remote_db_tables} > sync_backup.sql',
                [
                    'remote_db_user' => $remote_db_user,
                    'remote_db_pass' => $remote_db_pass,
                    'remote_db' => $remote_db,
                    'remote_db_tables' => $remote_db_tables,
                    'remote_db_host' => $remote_db_host,
                    'remote_db_port' => $remote_db_port ? $remote_db_port : 3306

                ]
            ));

        } else {

            $ssh->exec(self::format(
                'mysqldump -u {remote_db_user} -p{remote_db_pass} {remote_db} {remote_db_tables} > sync_backup.sql',
                [
                    'remote_db_user' => $remote_db_user,
                    'remote_db_pass' => $remote_db_pass,
                    'remote_db' => $remote_db,
                    'remote_db_tables' => $remote_db_tables,

                ]
            ));

        }

        // Connect via sftp to d/l the dump
        $sftp = new SFTP($remote_url);

        if (!$sftp->login($ssh_user, $ssh_pass)) {
            $this->error('Login failed make sure your SSH username and password is set in your env file . ');
            return;
        }

        // Temporarily remove memory limit
        ini_set('memory_limit', ' - 1');
        // Temporarily remove execution time
        set_time_limit(0);


        $this->info('Getting the backup . ');

        $sftp->get('sync_backup.sql', storage_path('sync_backup . sql'));

        $this->info('Importing...');
        DB::unprepared(File::get(storage_path('sync_backup.sql')));

        $this->info('Migrating...');
        $this->call('migrate');
        $this->info('Removing back up files . ');
        $ssh->exec('rm sync_backup.sql');
        File::delete(storage_path('sync_backup.sql'));
        $this->info('Complete!You are synced with the remote DB . ');
    }
}
