# Usage
This package allows you to sync your remote database to your local db for local dev purposes when trying to use live data.

+Added psr-4 fix, database tables to sync and database auth, host/ip

Original package: https://github.com/MidwesternInteractive/laravel-db-sync 

# Set Up
Add the following environment variables to your .env file ( LOCAL ONLY )
```
REMOTE_SYNC_URL=
REMOTE_SYNC_DB_NAME=

# tables to sync (optional) default: all tables
REMOTE_SYNC_DB_TABLES=

# database ip port (optional) default: localhost socket/3306
REMOTE_SYNC_DB_HOST=
REMOTE_SYNC_DB_PORT=

#dababase user and password
REMOTE_SYNC_DB_USER=
REMOTE_SYNC_DB_PASS=

#ssh auth
REMOTE_SYNC_SSH_USERNAME=
REMOTE_SYNC_SSH_PASSWORD=
```

## Install
```
add to composer.json
 "repositories": [
      {
        "type": "package",
        "package": {
          "name": "mwi/laravel-db-sync",
          "version": "1",
          "type": "package",
          "source": {
              "url": "https://pashkasan@bitbucket.org/pashkasan/laravel-db-sync.git",
              "type": "git",
              "reference": "master"
          }
        }
      }
    ],

run composer require mwi/laravel-db-sync
```

## Artisan Command
```shell
php aritsan dbsync
```